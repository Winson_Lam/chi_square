import numpy as np
import pandas as pd
import scipy

class create_frequencyTable_and_expectedFrequencyTable(object):
    def __init__(self, X_train, y_train):
        self.X_train = X_train
        self.y_train = y_train
        self.X_features = np.unique(X_train)
        self.y_features = np.unique(y_train)
        
        frequency_matrix = np.zeros([len(np.unique(self.X_train)), len(np.unique(self.y_train))])
        for i in range(self.X_train.shape[0]):
            row = self.X_train[i]
            for j in row:
                item_x = j
                item_y = self.y_train[i]

                index_axis0 = np.argwhere(np.unique(X_train) == item_x)[0][0]
                index_axis1 = np.argwhere(np.unique(y_train) == item_y)[0][0]
                frequency_matrix[index_axis0, index_axis1] += 1
        
        self.frequency_matrix = frequency_matrix

        x = np.array([np.sum(frequency_matrix, axis=0)])
        y = np.array([np.sum(frequency_matrix, axis=1)])
        expected_frequency = (x.T.dot(y).T/np.sum(frequency_matrix))
        self.expected_frequency = expected_frequency
    
        chi_square = np.sum((frequency_matrix - expected_frequency)**2/expected_frequency)
        self.chi_square = chi_square

X_train = np.array([['Nathan', 'Winson', 'Robin', 'Ally', 'Richard'], 
        ['Winson', 'Markus', 'James', 'Gareth', 'Robin'],
        ['Nathan', 'Ally', 'Winson', 'Gareth', 'Robin'],
        ['Ally', 'Winson', 'Gareth', 'Robin', 'Markus'],
        ['Markus', 'Gareth', 'Robin', 'James', 'Ally'],
        ['Winson', 'Ally', 'Gareth', 'Richard', 'James'],
        ['Nathan', 'Ally', 'Richard', 'Markus', 'Winson'], 
        ['Ally', 'Gareth', 'James', 'Winson', 'Markus'],
        ['Richard', 'Gareth', 'Winson', 'Markus', 'Robin'], 
         ['Winson', 'Nathan', 'Ally', 'Richard', 'Robin'],
         ['Richard', 'Ally', 'Winson', 'Robin', 'Markus'],
         ['Gareth', 'Markus', 'Robin', 'Ally', 'Richard']])

y_train = np.array(['won', 'lose', 'won', 'won', 'lose', 'lose', 'won', 'draw', 'draw', 'won', 'draw', 'draw'])

### Chi Square  - UDF and cross-validated with scipy

tables = create_frequencyTable_and_expectedFrequencyTable(X_train, y_train)

tables.frequency_matrix

tables.expected_frequency

tables.chi_square

scipy.stats.chi2_contingency(tables.frequency_matrix)
